import os
import csv
import logging
import pytz
import shutil
import MySQLdb
import numpy as np
import pickle


DB_HOST = "dbactive.bmat.srv"
DB_NAME = "vericast"


def connect_db(user, passwd):
    return MySQLdb.connect(host=DB_HOST, user=user, passwd=passwd,
                           db=DB_NAME)


def programs_to_dict(programs, query_type, main_category, media_type_id):
    dict = {}
    for program in programs:
        key = int(program[0])
        dict[key] = {}
        dict[key]['start_time'] = program[1]
        dict[key]['end_time'] = program[2]
        dict[key]['program_name'] = program[3]
        dict[key]['country'] = program[7]
        dict[key]['channel_id'] = program[5]
        dict[key]['channel_name'] = program[6]                
        dict[key]['main_category'] = main_category
        dict[key]['media_type_id'] = media_type_id
        if query_type == 0:
            dict[key]['category'] = program[4]

    return dict


def delete_programs_without_recordings(programs):
    for id in programs.keys():
        if not 'recordings' in programs[id]:
            del programs[id]

    return programs


def add_recordings_path_to_programs_dict(recordings, programs):
    for recording in recordings:
        if not 'recordings' in programs[int(recording[0])]:
            programs[recording[0]]['recordings'] = {}
        programs[recording[0]]['recordings'][recording[1]] = {}
        dict = {}
        dict['recording_path'] = recording[2]
        dict['start_time'] = recording[3]
        dict['end_time'] = recording[4]
        programs[recording[0]]['recordings'][recording[1]] = dict

    return programs


def save_programs(programs, country, channel_ids, categories, main_category,
                  query_type):
    if os.path.exists("../metadata/metadata.p"):
        with open("../metadata/metadata.p", 'r') as pickle_file:
            metadata = pickle.load(pickle_file)
    else:
        metadata = {}
    channels = []
    for program in programs.values():
        if not program['channel_name'] in channels:
            channels.append(program['channel_name'])
    if not country in metadata.keys():
        metadata[country] = {}
    metadata[country][main_category] = {}
    metadata[country][main_category]['programs'] = {}
    metadata[country][main_category]['used_channels'] = channels
    if query_type == 0:
        if categories[-1] == 'no_category':
            categories.pop(-1)
        metadata[country][main_category]['used_categories'] = categories
    for id in programs.keys():
        metadata[country][main_category]['programs'][id] = programs[id]
    with open("../metadata/metadata.p", 'w') as pickle_file:
        pickle.dump(metadata, pickle_file)


def save_programs_by_channel(recordings, country, channel_ids):
    if os.path.exists("../metadata/metadata.p"):
        with open("../metadata/metadata.p", 'r') as pickle_file:
            metadata = pickle.load(pickle_file)
    else:
        metadata = {}
    if not country in metadata.keys():
        metadata[country] = {}
    dict = {}
    for recording in recordings:
        dict[recording[0]] = {}
        dict[recording[0]]['path'] = recording[1]
        dict[recording[0]]['start_time'] = recording[2]
        dict[recording[0]]['end_time'] = recording[3]
        dict[recording[0]]['channel_id'] = recording[4]
        dict[recording[0]]['channel_name'] = recording[5]
    metadata[country]['recordings'] = dict

    with open("../metadata/metadata.p", 'w') as pickle_file:
        pickle.dump(metadata, pickle_file)


def get_programs(country, channel_ids, categories, main_category,
                 start_time_limit, media_type_id, num_prog, query_type,
                 mysql_user, mysql_pass):
    if len(channel_ids) == 1:
        channel_ids.append(-1)
    if len(categories) == 1:
        categories.append('no_category')

    if query_type == 0:
        query = ("select distinct epa.id, epa.start_time, epa.end_time, "
                 "ep.name, epa.category, c.id, c.name, co.name from "
                 "epg_program_aired epa join epg_program ep on ep.id = "
                 "epa.program_id join channel c on c.id in {channel_ids} join "
                 "country co where epa.category in {categories} and co.name = "
                 "'{country}' and epa.channel_id = c.id and epa.start_time > "
                 "'{start_time_limit}' and c.media_type_id = {media_type_id} "
                 "order by rand() limit {num_prog};")
        query = query.format(
                    country=country,
                    channel_ids=tuple(channel_ids),
                    categories=tuple(categories),
                    start_time_limit=start_time_limit,
                    num_prog=num_prog,
                    media_type_id=media_type_id)
        print "\n" + query

    elif query_type == 1:
        query = ("select distinct epa.id, epa.start_time, epa.end_time, "
                 "ep.name, epa.category, c.id, c.name, co.name from "
                 "epg_program_aired epa join epg_program ep on ep.id = "
                 "epa.program_id join channel c on c.id in {channel_ids} join "
                 "country co where co.name = '{country}' and epa.channel_id = "
                 "c.id and epa.start_time > '{start_time_limit}' "
                 "and c.media_type_id = {media_type_id} order by rand() limit "
                 "{num_prog};")
        query = query.format(
                    country=country,
                    channel_ids=tuple(channel_ids),
                    start_time_limit=start_time_limit,
                    num_prog=num_prog,
                    media_type_id=media_type_id)
        print "\n" + query

    programs = []
    db = None
    try:
        logging.debug(query)
        db = connect_db(mysql_user, mysql_pass)
        db.query(query)
        r = db.store_result()
        programs = r.fetch_row(maxrows=0)
    except Exception, e:
        logging.error(e)
    finally:
        if db:
            db.close()

    programs = programs_to_dict(
                    programs,
                    query_type,
                    main_category,
                    media_type_id)

    return programs


def get_programs_recordings_path(programs, media_type_id, mysql_user,
                                 mysql_pass):
    programs_ids = programs.keys()
    if media_type_id == 1:
        extension = '.mp4'
    else:
        extension = '.mp3'

    if len(programs_ids) == 1:
        programs_ids.append(-1)
    
    query = ("select epa.id, r.id, r.path, r.start_time, r.end_time from "
             "recording r join epg_program_aired epa on r.channel_id = "
             "epa.channel_id where epa.id in {programs_ids} and r.start_time "
             "< epa.end_time and r.end_time > epa.start_time and r.available "
             "= 1 and r.path like '%{extension}%';")
    query = query.format(
                programs_ids=tuple(programs_ids),
                extension=extension)
    print "\n" + query

    recordings = []
    db = None
    missing_programs = 0
    try:
        logging.debug(query)
        db = connect_db(mysql_user, mysql_pass)
        db.query(query)
        r = db.store_result()
        recordings = r.fetch_row(maxrows=0)
    except Exception, e:
        logging.error(e)
    finally:
        if db:
            db.close()

    return recordings


def get_programs_recordings_path_by_channel(channel_ids, save_num_prog,
                                            media_type_id, start_time_limit,
                                            mysql_user, mysql_pass):
    if media_type_id == 1:
        extension = '.mp4'
    else:
        extension = '.mp3'

    if len(channel_ids) == 1:
        channel_ids.append(-1)

    query = ("select r.id, r.path, r.start_time, r.end_time, r.channel_id, "
             "c.name from recording r join channel c on c.id = r.channel_id"
             " where c.id in {channel_ids} and r.available = 1 and r.path "
             "like '%{extension}%' and r.start_time > '{start_time_limit}' "
             "order by rand() limit {save_num_prog};")
    query = query.format(
                channel_ids=tuple(channel_ids),
                extension=extension,
                save_num_prog=save_num_prog,
                start_time_limit=start_time_limit)

    recordings = []
    db = None
    missing_programs = 0
    try:
        logging.debug(query)
        db = connect_db(mysql_user, mysql_pass)
        db.query(query)
        r = db.store_result()
        recordings = r.fetch_row(maxrows=0)
    except Exception, e:
        logging.error(e)
    finally:
        if db:
            db.close()

    return recordings


def pick(country, start_time_limit, save_num_prog, query_num_prog, mysql_user,
         mysql_pass, media_type_id, channel_ids, categories=[],
         main_category=None):  
    query_type = -1
    if channel_ids != [] and categories != []:
        print("Query by channels and categories.")
        query_type = 0
    elif categories == []:        
        print("Query by channels only.")
        query_type = 1
    else:
        raise ValueError('Specify either channel ids or categories or both')
    
    print("Searching for programs.")
    programs = get_programs(
                    country,
                    channel_ids,
                    categories,
                    main_category,
                    start_time_limit,
                    media_type_id,
                    query_num_prog,
                    query_type,
                    mysql_user,
                    mysql_pass)

    print("Retrieving corresponding recordings.")
    recordings = get_programs_recordings_path(
                    programs,
                    media_type_id,
                    mysql_user,
                    mysql_pass)

    programs = add_recordings_path_to_programs_dict(
                    recordings,
                    programs)

    programs = delete_programs_without_recordings(programs)

    final_programs = {}
    if len(programs.keys()) > 0:
        for id in programs.keys():
            if not id in final_programs.keys():
                final_programs[id] = programs[id]
                if len(final_programs.keys()) == save_num_prog:
                    save_programs(
                        final_programs,
                        country,
                        channel_ids,
                        categories,
                        main_category,
                        query_type)
                    print("All programs found. Saving metadata.")
                    return 0

    print "  -  only %d programs saved" % len(final_programs.keys())
    save_programs(
        final_programs,
        country,
        channel_ids,
        categories,
        main_category,
        query_type)
    return -1


def pick_recordings_by_channel(country, start_time_limit, save_num_prog,
                               mysql_user, mysql_pass, media_type_id,
                               channel_ids=[]):
    if media_type_id != 1 and media_type_id != 3:
        raise ValueError('Not a valid media_type_id value')
    print("Retrieving recordings.")
    recordings = get_programs_recordings_path_by_channel(
                    channel_ids,
                    save_num_prog,
                    media_type_id,
                    start_time_limit,
                    mysql_user,
                    mysql_pass)
    if len(recordings) == save_num_prog:
        print("All programs found. Saving metadata.")
        save_programs_by_channel(recordings, country, channel_ids)
        return 0
    else:
        print "  -  %d recordings missing" % (save_num_prog - len(recordings))
        print("No metadata saved.")
        return -1
