import subprocess
import pickle
import os
import random
import datetime


def download_and_trim_recordings(metadata, audio_output_dir, tmp_dir):
    for country in metadata.keys():
        print "\n  -  Country: " + country
        for main_category in metadata[country].keys():
            print "\n  -  Main category: " + main_category
            prog_dict = metadata[country][main_category]['programs']
            for id in prog_dict.keys():
                print "\n  -  Program ID: " + str(id)
                start_time = prog_dict[id]['start_time']
                duration = (prog_dict[id]['end_time'] - start_time).seconds
                rec_dict = prog_dict[id]['recordings']
                rec_id, recording = random.choice(rec_dict.items())
                while recording['end_time'] - prog_dict[id][
                    'start_time'] < datetime.timedelta(0, 61) or prog_dict[id][
                    'end_time'] - recording['start_time'] < datetime.timedelta(
                    0, 61):
                    rec_id, recording = random.choice(rec_dict.items())
                path = recording['recording_path']
                fname = tmp_dir + path.split('/')[-1]
                print "\n    -  Downloading recording..."
                download_recordings(path, fname, tmp_dir)
                print "\n    -  Creating excerpt..."
                program = trim_program(prog_dict[id], id, recording, rec_id,
                            audio_output_dir, tmp_dir, duration)
                prog_dict[id] = program
                clean_tmp_dir(tmp_dir)
            print "\n  -  Updating metadata..."
            metadata[country][main_category]['programs'] = prog_dict

    return metadata


def download_recordings(path, fname, tmp_dir):
    fname_wav = fname.replace('.mp4', '.wav')
    subprocess.call(['wget', '-P', tmp_dir,
        'http://recsproxy.bmat.com' + '/' + '/'.join(path.split('/')[3:])])
    subprocess.call(['ffmpeg', '-i', fname, fname_wav])


def trim_program(program, program_id, rec, rec_id, audio_output_dir, tmp_dir,
                 duration):
    time_range = rec['end_time'] - rec['start_time']
    offset = datetime.timedelta(0, 0)
    if rec['start_time'] < program['start_time']:
        offset = program['start_time'] - rec['start_time']
        time_range -= offset
    if rec['end_time'] > program['end_time']:
        upper_waste = rec['end_time'] - program['end_time']
        time_range -= upper_waste
    time_range = time_range.total_seconds()
    excerpt_start_time = random.uniform(
            0, time_range - datetime.timedelta(0, 61).total_seconds())
    excerpt_start_time += offset.total_seconds()
    excerpt_start_time = int(excerpt_start_time)
    country = program['country']
    main_category = program['main_category']
    program['excerpt'] = {}
    program['excerpt']['recording_id'] = rec_id
    program['excerpt']['start_time'] = rec[
        'start_time'] + datetime.timedelta(0, excerpt_start_time)
    program['excerpt']['end_time'] = program[
        'excerpt']['start_time'] + datetime.timedelta(0, 60)
    fname = audio_output_dir
    fname += country + '_' + main_category + '_' + str(program_id) + '_' 
    fname += str(rec_id) + '.wav'
    program['excerpt']['excerpt_path'] = fname
    subprocess.call(['ffmpeg', '-ss', str(excerpt_start_time), '-t', '60',
        '-i', tmp_dir + rec['recording_path'].split('/')[-1].replace(
                                                    '.mp4', '.wav'), fname])
    
    return program

def clean_tmp_dir(tmp_dir):
    if os.path.exists(tmp_dir):
        subprocess.call(['rm', '-rf', tmp_dir])
    subprocess.call(['mkdir', tmp_dir])


def generate(metadata_file, audio_output_dir, tmp_dir,
             specify_country=["all"]):
    with open(metadata_file, 'r') as pickle_file:
        metadata = pickle.load(pickle_file)

    if "all" in specify_country:
        metadata = download_and_trim_recordings(metadata, audio_output_dir,
                        tmp_dir, seconds_per_excerpt, excerpts_per_program)    
    else:
        for country in specify_country:
            tmp_dict = {}
            tmp_dict[country] = metadata[country]
            metadata = download_and_trim_recordings(
                            tmp_dict,
                            audio_output_dir,
                            tmp_dir)

    with open(metadata_file, 'w') as pickle_file:
        pickle.dump(metadata, pickle_file)
